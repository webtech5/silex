<?php
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

$dbOptions = [
    'driver'   => 'pdo_mysql',
    'dbname'     => 'silex_onlineshop',
    'host' => 'localhost',
    'port' => 3306,
    'user' => 'dev',
    'password' => 'dev'
];

$app = new Application();
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $dbOptions,
    'debug' => true
));

$app->get('/hello/{name}', function($name) use ($app) {
    return 'Hi' . $name;
});

$app->get('/user/{id}', function ($id) use ($app) {
    $sql = 'SELECT * FROM user WHERE id = "' . $id . '"';
    $user = $app['db']->fetchAssoc($sql);
    if (empty($user)) {
        $app->abort(404);
    }
    return json_encode($user);
});

$app->post('/user', function (Request $request) use ($app) {
    $sql = 'INSERT INTO user (email, password) 
      VALUES ("' . $request->get('email') . '","' . $request->get('password') . '")';
    $app['db']->query($sql);
    return json_encode(['success' => true]);
});

$app->put('/user', function (Request $request) use ($app) {
    $sql = 'UPDATE user 
      SET 
        email = "' . $request->get('email') . '",
        password = "' . $request->get('password') . '"
    WHERE id = "' . $request->get('id') . '"';
    $success = $app['db']->query($sql)->execute();
    return json_encode(['success' => $success]);
});

$app->delete('/user/{id}', function ($id) use ($app) {
    $sql = 'DELETE FROM user
    WHERE id = ' . $id;
    $success = $app['db']->query($sql)->execute();
    return json_encode(['success' => $success]);
});

$app->post('/login', function (Request $request) use ($app) {
    $sql = 'SELECT * 
      FROM user 
      WHERE email = "' . $request->get('email') . '"
        AND password = "' . $request->get('password') . '"';
    $post = $app['db']->fetchAssoc($sql);
    return json_encode(['success' => !empty($post)]);
});

$app->get('/product', function () use ($app) {
    $sql = 'SELECT * FROM product';
    $products = $app['db']->fetchAll($sql);
    return json_encode($products);
});

$app->get('/product/{id}', function ($id) use ($app) {
    $sql = 'SELECT * FROM product WHERE id = "' . $id . '"';
    $product = $app['db']->fetchAssoc($sql);
    return json_encode($product);
});

$app->delete('product/{id}', function ($id) use ($app) {
    $sql = 'DELETE FROM product WHERE id = "' . $id . '"';
    $app['db']->query($sql);
    return json_encode(['success' => true]);
});

$app->run();