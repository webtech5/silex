# Installation

- [Composer](https://getcomposer.org/doc/00-intro.md) installieren 
- Das Zeug hier auf den Webserver packen, 
z.B. mit Xampp in C:\xampp\htdocs\silex
- dann mit bash/shell in den Ordner navigieren z.B. `cd /c/xampp/htdocs/silex`
- `composer install` um sog. Dependencies zu installieren (man kann in der composer.json nachsehen welche das sind. Die liegen danach in dem Ordner "vendor")
- Testen (im Browser z.B: http://localhost/silex/hello/asdf)
- in index.php die Zugangsdaten für die Datenbank eintragen
- get/post/put/delete Funktionen an eigene Anforderungen anpassen